const path = require('path');
const webpack = require('webpack');

module.exports = {
  devtool: 'eval',
  entry: './src/app',
  output: {
    path: path.join(__dirname, 'dist'),
    filename: 'bundle.js'
  },
  module: {
    loaders: [
      {
        test: /\.js$/,
        loaders: ['babel']
      },
      {
        test: /\.html$/,
        loaders: ['html']
      }
    ],
    preLoaders: [
      {
        test: /\/src\/elements\/.+\.js$/,
        loader: 'polymer-loader?templateExtension=html&styleExtension=css'
      }
    ]
  },
  devServer: {
    colors: true,
    historyApiFallback: true,
    inline: true
  }
};
